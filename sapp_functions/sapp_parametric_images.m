function sapp_parametric_images(subject_info, params)

% NOTE!: If we want to use the PET 0.7mm3, this is failing when the ROI is calculated for ratio images!!!!!!!

pet_file     = sprintf('%s\\rs02_PT_%s_head.nii', subject_info.path, subject_info.name);


% TO DO: check first if a parametric image as the one indicated already
% exists before computing it (in the case the user only wants to create a
% static with different time frames

% TO DO: if what the user wants is ratio, we don't need to compute SUV
% image first, but just ratio image

sapp_calculate_parametric_images(pet_file, subject_info, params);


% if user wants single static parametric img AND gave number of frames AND
% the PET is dynamic
if (params.staticParametricImage) && (~isnan(params.parametricFrames(1))) && strcmp(subject_info.dyn, 'dynamic')
    
    if params.parametricImageType == 1
        parametric_file = spm_file(pet_file, 'suffix', ['_', 'SUV']);
    elseif params.parametricImageType == 2
        parametric_file = spm_file(pet_file, 'suffix', ['_', 'IDg']);
    end
    
    Vi = spm_vol(parametric_file);
    Vo = spm_file(parametric_file, 'prefix','Static_');
    
    Vi = Vi(params.parametricFrames(1) : params.parametricFrames(2));
    
    f= 'mean(X)';
    flags = {1, 0, 1, 4};  %{dmtx,mask,interp,dtype,descrip}
    
    Vout = spm_imcalc(Vi, Vo, f, flags);

end