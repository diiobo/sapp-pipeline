function sapp_convert_to_nifti(source_dir,nii_dir,filename)
% The function assumes that source_dir contains only a bunch of dicom files
% These files are then converted into a single (4D when applicable) nifti

% Modified from Tomi
%------------------------------

[p,n,e] = fileparts(filename);   % path, name, extension

if(isempty(p))
    p = nii_dir;
end
if(isempty(e))
    e = '.nii';
end
filename = fullfile(p,[n e]); % create full path

f = get_filenames(source_dir,'*.dcm');   % sort_nat files??

if(~isempty(f))
    nii_fnames = spm_dcm2nii(source_dir,nii_dir);   %nii_fnames is struct
    if(length(nii_fnames.files)==1)
        movefile(char(nii_fnames.files),filename,'f');
    else
        spm_nifti_dynamize(nii_fnames.files,filename); % converts from 3d to 4d
    end
else
    error('Could not find dicom files from %s.\n',source_dir);
end

end