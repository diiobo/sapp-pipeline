function [crop_coord] = get_crop_coord_ct(X1, X2, Y1, Z1, species)
    
% [X1 X2    --> always given
%  Y1 Y2  
%  Z1 Z2]

if species == 'M'
    vol_sz = 110; % pixels, TODO: take into account pixel size, use cm better!   
    Y1 = Y1 + 50;

elseif species == 'R'   % centrar mas los cerebros de las ratas que estan solas
    vol_sz = 200;
    Y1 = Y1 + 70;
end
    
Y2 = Y1 + vol_sz - 1;
Z2 = Z1 + vol_sz - 1;

if (X2 - X1) < vol_sz
    dif = floor((vol_sz - (X2-X1))/2);
    X1 = X1 - dif;
    X2 = X2 + dif;
elseif (X2 - X1) > vol_sz
    dif = floor(((X2-X1)-vol_sz)/2);
    X1 = X1 + dif;
    X2 = X2 - dif;
end

% this to avoid error when x1 or x2 are outside the image size. TO DO: pad
% the empty columns better!
% if X1 < 1,    X1=1;     end
% if X2 > 384,  X2 = 384; end
% if Y2 > 384,  Y2 = 384; end
% if Z1 < 1,    Z1=1;     end
% if Z2 > 384,  Z2 = 384; end


          
crop_coord = [X1 X2; ...
              Y1 Y2; ...
              Z1 Z2];
          
% corners = [
% X1    Y1    Z1    1
% X1    Y1    Z2    1
% X1    Y2    Z1    1
% X1    Y2    Z2    1
% X2    Y1    Z1    1
% X2    Y1    Z2    1
% X2    Y2    Z1    1
% X2    Y2    Z2    1
% ]';
          
end
              



    
