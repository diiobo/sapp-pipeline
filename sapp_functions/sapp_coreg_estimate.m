function T_coreg = sapp_coreg_estimate(ref_img, source_img, second_round)
%
% Estimates the coregistration affine matrix and stores it as .mat
% - no reslicing
% - writes the affine matrix to the source_img header
% TO DO: generalize for case source_img >1 

% Co-registration flags
coreg_flags.coreg.estimate.cost_fun = 'nmi';
coreg_flags.coreg.estimate.sep      = [0.4 0.2];
coreg_flags.coreg.estimate.tol      = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
coreg_flags.coreg.estimate.fwhm     = [5 5];


% coreg_flags.coreg.write.mask        = 0;
% coreg_flags.coreg.write.mean        = 0;
% coreg_flags.coreg.write.interp      = 1; %4
% coreg_flags.coreg.write.which       = 1;
% coreg_flags.coreg.write.wrap        = [0 0 0];
% coreg_flags.coreg.write.prefix      = 'r';

% If this is a second-round PET-CT co-reg, do it with a fine 'sep' parameter
if second_round
    disp('Sanity check: Inside second round!')
    coreg_flags.coreg.estimate.sep  = [0.1];
end


% call directly to spm_coreg function
x = spm_coreg(ref_img, source_img, coreg_flags.coreg.estimate);   % x = vector of parameters P
T_coreg = spm_matrix(x);

% Transform the affine matrix of source_img (meanpet) to the space of ref_img (ct)
Affine = spm_get_space(source_img);
new_Affine = T_coreg \ Affine;

% Write the affine matrix in source_img (meanpet) header! this is needed to visualize the fig in QC (ct + co-reg pet)
spm_get_space(source_img, new_Affine);


%  save T_coreg, TAKE THIS OUT OF HERE
[p, ~, ~] = fileparts(ref_img);
if ~isdir(sprintf('%s\\TR',p)), mkdir(sprintf('%s\\TR',p)); end
save([sprintf('%s\\TR',p),'\\PETtoCT_coreg.mat'], 'T_coreg');


end

