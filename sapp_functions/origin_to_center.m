function origin_to_center(file)

[p,n,e] = fileparts(file);  % path, name, extension
files = cellstr(spm_select('ExtFPList',p,[n e], Inf));

for i=1:numel(files)
    file = deblank(files{i});
    v = spm_vol(file);
    
    vs = v.mat\eye(4);
    vs(1:3,4) = floor((v.dim)/2);
    new_aff = inv(vs);
    
    spm_get_space(files{i}, new_aff);
end


end

