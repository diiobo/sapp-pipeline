function ratios = calculate_ratios( data, refROIs)
% assumes data is the matrix returned by calculate_voi_analysis.m where
% data is of size nfiles * VOIs
% refROIs is a vector containing the number of the ROIs used as reference

% ratios is a nfiles * VOIs * # number of reference regions, where each
% plane correspons to all the data divided by the ref. region

ratios = zeros([size(data), length(refROIs)]);

    for i = 1:length(refROIs)
        ratios(:,:,i) = data ./ data(:,refROIs(i));
    end    


end