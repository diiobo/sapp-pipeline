function sapp_coreg_qc(subject_dir, ct_image, pet_image)
% Visualizes orthogonal slices of PET and MR images. The MRI is shown as a
% red overlay on top of the PET image. The visualization is saved to the
% subject's qc file.
% FROM Tomi



% Define values related to positioning of the slices
d = 0.05;
w = 0.8/3;
h = 0.8;
y = 0.1;

N = 5; % number of slices

% Define the slices (in millimiters) that are shown
xx = [-2 -1 0 1 2];
yy = [-2 -1 0 1 2];
zz = [-2 -1 0 1 2];

ff = figure('Position',[10 10 200 200],'Visible','Off');

for i = 1:N
    
    fig = spm_figure('Create','Graphics','','off');
    fig.Position = [796 20 1200 400];
    
    clear global st
    
    k = spm_orthviews('image',ct_image);
    % spm_orthviews('AddColouredImage',k,pet_image,[1 0 0]);
    cmap = jet;
    spm_orthviews('addtruecolourimage', k, pet_image, cmap, 0.2);  % this works ok because I disabled "addcolourbar" from
    % addtrucolourimage function
        
    %spm_orthviews('Xhairs','off')
    spm_orthviews('reposition',[xx(i) yy(i) zz(i)]);
    spm_orthviews('redraw');
    
    ax = fig.Children;
    
    ax(1).Position(1) = d;
    ax(2).Position(1) = 2*d+w;
    ax(3).Position(1) = 3*d+2*w;
    
    ax(1).Position(2) = y;
    ax(2).Position(2) = y;
    ax(3).Position(2) = y;
    
    ax(1).Position(3) = w;
    ax(2).Position(3) = w;
    ax(3).Position(3) = w;
    
    ax(1).Position(4) = h;
    ax(2).Position(4) = h;
    ax(3).Position(4) = h;
    
    figstruct = getframe(fig);
    close(fig);
    X = figstruct.cdata;
    
    subplot(N,1,i); imshow(X);
    if i == 1
       title('PET Co-registered to CT');
    end

    
end

sapp_add_to_qc_pic(subject_dir,ff)
close(ff);

end