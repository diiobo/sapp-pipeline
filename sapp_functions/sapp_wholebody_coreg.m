function sapp_wholebody_coreg(data_set, top_dir, subjects_info)
% Co-register the whole dataset (whole body) PET to CT

data_dir = sprintf('%s\\%s',top_dir,data_set);

ct_dir = sprintf('%s\\CT_%s',data_dir, data_set);
pet_dir = sprintf('%s\\PT_%s',data_dir, data_set);

ct_file = sprintf('%s.nii',ct_dir);     % one 3D NIFTI file
pet_file = sprintf('%s.nii',pet_dir);   % 4D NIFTI


%% Set the Origin at the Center of the image for both CT and PET

origin_to_center(ct_file);
origin_to_center(pet_file);


%% Co-register Whole Body PET to CT

% If dynamic, create MeanPET, otherwise duplicate
if subjects_info(1).dyn == 'dynamic'
    meanPET = mean_pet(pet_file); 
elseif subjects_info(1).dyn == 'static'
    meanPET = sprintf('%s\\copy_PT_%s.nii', data_dir, data_set);
    copyfile(pet_file, meanPET);
end


T = sapp_estimate_initial_coreg(ct_file, meanPET);

% apply to whole PET
Affine = spm_get_space(pet_file);
new_Affine = T\ Affine;

[p,n,e] = fileparts(pet_file);
files = cellstr(spm_select('ExtFPList',p,[n e], Inf));
for i=1:numel(files)
    spm_get_space(files{i}, new_Affine);
end

delete(meanPET);

delete(sprintf('%s.mat',pet_dir)) %if exists?


end
