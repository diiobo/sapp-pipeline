function extract_head(data_set, subjects_info, top_dir, species)
% Extracts the heads of the subjects present in the dataset


ct_file   = sprintf('%s\\%s\\CT_%s.nii', top_dir, data_set, data_set);  % one 3D NIFTI file
pet_file  = sprintf('%s\\%s\\PT_%s.nii', top_dir, data_set, data_set); % 4D NIFTI


V      = spm_vol(ct_file);
ct_vol = spm_read_vols(V);


% Find Y_coord by detecting the bed 
[Ycoord, lateral_Xcoords] = find_Ycoord(ct_file);


% Find X_coord if necessary --> check number of subjects in the FOV
[Xcoord] = find_Xcoord(ct_file); % Xcoord = 0 if only 1 animal


% Find Zcoord and crop the head(s)
if (Xcoord == 0) && (length(subjects_info)==1) 
    
    % 1 animal
    fprintf('1 subject found...extracting head now\n');

    [~,n,~]    = fileparts(subjects_info.path);
    ct_fname   = sprintf('%s\\CT_%s_head.nii', subjects_info.path , n);
    pet_fname  = sprintf('%s\\PT_%s_head.nii', subjects_info.path , n);

    Zcoord          = find_Zcoord(ct_vol);  
    Zcoord   = Zcoord - 5;   % in Mice it tends to cut too close to Olfactory Bulbs so better be safe
    
    crop_coord_ct   = get_crop_coord_ct(lateral_Xcoords(1), lateral_Xcoords(2), Ycoord, Zcoord, species);
    crop_head(ct_file, crop_coord_ct, ct_fname);
    
    crop_coord_pet  = get_crop_coord_pet(ct_file, pet_file, crop_coord_ct);
    crop_head(pet_file, crop_coord_pet, pet_fname);

        
elseif (Xcoord ~= 0) && (length(subjects_info) == 2) 
    
    % 2 animals
    fprintf('2 subjects found...extracting heads now\n');

    
    % Subject 1  (Right)
    
    [~,n1,~]     = fileparts(subjects_info(1).path);
    ct_fname_R   = sprintf('%s\\CT_%s_head.nii', subjects_info(1).path, n1);
    pet_fname_R  = sprintf('%s\\PT_%s_head.nii', subjects_info(1).path, n1);
    
    Zcoord1  = find_Zcoord(ct_vol(lateral_Xcoords(1):Xcoord,:,:));
    Zcoord1   = Zcoord1 - 5; 
    
    crop_coord_ct_R  = get_crop_coord_ct(lateral_Xcoords(1), Xcoord, Ycoord, Zcoord1, species);
    crop_head(ct_file, crop_coord_ct_R, ct_fname_R);
    
    crop_coord_pet_R = get_crop_coord_pet(ct_file, pet_file, crop_coord_ct_R);
    crop_head(pet_file, crop_coord_pet_R, pet_fname_R);

    
    % subject 2  (Left)

    [~,n2,~]    = fileparts(subjects_info(2).path);
    ct_fname_L  = sprintf('%s\\CT_%s_head.nii', subjects_info(2).path, n2);
    pet_fname_L = sprintf('%s\\PT_%s_head.nii', subjects_info(2).path, n2); 
    
    Zcoord2  = find_Zcoord(ct_vol(Xcoord:lateral_Xcoords(2),:,:));
    Zcoord2   = Zcoord2 - 5; 
    
    crop_coord_ct_L  = get_crop_coord_ct(Xcoord, lateral_Xcoords(2), Ycoord, Zcoord2, species);
    crop_head(ct_file, crop_coord_ct_L, ct_fname_L);
    
    crop_coord_pet_L = get_crop_coord_pet(ct_file, pet_file, crop_coord_ct_L);
    crop_head(pet_file, crop_coord_pet_L, pet_fname_L);
    
else
    
    error('Error: there was an incongruency between the number of subjects specified and number of subjects detected')
    
end