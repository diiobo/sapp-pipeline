function sapp_single_subject_preprocess (subject_info, params)

species = params.Species.species;
rigid = params.rigid;

subject_dir  = subject_info.path;

ct_head = sprintf('%s\\CT_%s_head.nii',subject_dir, subject_info.name);
pet_head = sprintf('%s\\PT_%s_head.nii',subject_dir,subject_info.name);



%% Load atlas paths

sapp_def      = sapp_defaults(params.Species);
ct_ref        = sapp_def.ct_ref;
boundaries    = sapp_def.bound;
atlas         = sapp_def.atlas; 
vois          = sapp_def.voi;

clear sapp_def;


%% If dynamic, calculate meanPET to use in the co-registration


if strcmp(subject_info.dyn, 'dynamic')
    meanPET = mean_pet(pet_head);
else   %duplicate
    meanPET = sprintf('%s\\copy_PT_%s_head.nii',subject_dir,subject_info.name);
    copyfile(pet_head, meanPET);
end



%% QC: cropped head
% (use meanPET at initial position)

sapp_crop_qc(subject_dir, ct_head, meanPET);



%% Co-registration of meanPET to CT

T_coreg = sapp_coreg_estimate(ct_head, meanPET, 0);  % also stores the transf. matrix as .mat


%QC: Co-registration : if second round later, this doesn't make sense anymore here!
sapp_coreg_qc(subject_dir, ct_head, meanPET);


%% Set Origin at estimated location of the brain

T_com = sapp_cg_set_com_mod(ct_head, species);


% QC: Brain localization and initial alignment to reference CT
sapp_com_qc(subject_dir, ct_head, ct_ref);


%% Spatial normalization of Subject CT to reference CT 

T_norm = sapp_spatial_normalization(ct_ref, ct_head, rigid);



%% Apply all transformations to PET file

pet_affine = spm_get_space(pet_head);

pet_new_affine = (T_coreg * T_com * T_norm) \ pet_affine; % Same: T_norm \ (T_com \ (T_coreg \ pet_affine))
%pet_new_affine = (T_coreg * T_norm) \ pet_affine;

[p,n,e] = fileparts(pet_head);  % path, name, extension
pet_files = cellstr(spm_select('ExtFPList',p,[n e], Inf));
for i=1:numel(pet_files)     % will include also the MeanPet file
    spm_get_space(pet_files{i}, pet_new_affine);
end

delete(sprintf('%s\\%s.mat',p,n))



%% Reslice CT and PET heads to apply transformation WITHOUT changing voxel size
% by using as reference image the original head image from the backup folder

ct_backup  = sprintf('%s\\Head_original\\CT_%s_head.nii', subject_dir, subject_info.name);
pet_backup = sprintf('%s\\Head_original\\PT_%s_head.nii,1', subject_dir, subject_info.name); %works if static???? TEST!

rsl_ct_head   = sapp_reslice(ct_backup, ct_head, 'rs', 3);
rsl_pet_head  = sapp_reslice(pet_backup, pet_head, 'rs', 1); % 1 = trilinear interpolation
rsl_meanPET   = sapp_reslice(pet_backup, meanPET, 'rs', 1);




%% Reslice the PET to the VOIs file voxel size (0.2 x 0.2 x 0.2 mm)
% this will be the PET used for measurements

vois1 = [vois ',1']; % select only one frame, if dynamic: sapp_reslice also writes a new target file (vois)
sapp_reslice(vois1, pet_head, 'rs02_', 1);



%% QC: spatial normalization

% VOIs boundaries on top of the CT
% sapp_alignment_qc(subject_dir, rsl_ct_head, atlas, species);

rsl_ct_head_small   = sapp_reslice(ct_ref, ct_head, 'small_bbox', 3); % to 0.1 mm for mouse
sapp_alignment2_qc(subject_dir, rsl_ct_head_small, atlas, species);
sapp_alignment2_qc(subject_dir, rsl_ct_head_small, boundaries, species);
delete(rsl_ct_head_small);


%% QC: PET-CT co-registered and aligned!
sapp_coreg_qc(subject_dir, rsl_ct_head, rsl_meanPET);



%% DELETE files and reorganize folder

delete(rsl_meanPET);

mkdir(sprintf('%s\\other',subject_dir));
movefile(ct_head, sprintf('%s\\other',subject_dir));
movefile(pet_head, sprintf('%s\\other',subject_dir));
movefile(meanPET, sprintf('%s\\other',subject_dir));




%% QC: convert PS file to PDF

sapp_ps2pdf(subject_dir);


end

