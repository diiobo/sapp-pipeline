function [frames_short, dyn] = get_dataset_tframe_info(dataset, top_dir)


data_dir = sprintf('%s\\%s',top_dir,dataset);
pet_dir = sprintf('%s\\PT_%s',data_dir, dataset);

tframe_info_file = [data_dir,'\PT_tframes_info.mat'];


if exist(tframe_info_file, 'file')
    
    load(tframe_info_file);
    
    if (size(frames_short,1) == 1) && (frames_short(1,1) == 1)
        dyn = 'static';
    else
        dyn = 'dynamic';
    end
    
elseif exist(pet_dir,'dir')
    
    [frames_short, dyn] = sapp_get_frames_dcm(pet_dir);
    save(tframe_info_file, 'frames_short');
    
else 
    error('Could not find raw PET data (dicom) from %s or the file containing the time frame info.',pet_dir);
end

end



