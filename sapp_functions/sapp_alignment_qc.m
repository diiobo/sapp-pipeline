function sapp_alignment_qc(subject_dir, ct_image, voi_image, species)

% Define values related to positioning of the slices
d = 0.05;
w = 0.8/3;
h = 0.8;
y = 0.1;


% Define the slices (in millimiters) that are shown
xx = [-1 0 1 2];
yy = [-1 0 1 2];
zz = [-1 0 2 2];
% xx = [0];
% yy = [0];
% zz = [0];



% Bounding box to display
if species == 'R'
    bb = [-75 -75 -75; 75 75 70];          % [loX loY loZ; hiX hiY hiZ]
else
    bb = [-50 -50 -50; 50 50 45];
end



N = length(xx); % number of slices

ff = figure('Position',[10 10 200 200],'Visible','Off');

for i = 1:N
    
    fig = spm_figure('Create','Graphics','','off');
    
    fig.Position = [796 20 1200 400];
    
    clear global st
    
    k = spm_orthviews('image',ct_image);
    spm_orthviews('AddColouredImage',k,voi_image,[0 1 0]);
%     spm_orthviews('Xhairs','off')
%     spm_orthviews('BB',bb)
    spm_orthviews('reposition',[xx(i) yy(i) zz(i)]);
    spm_orthviews('redraw');
    
    ax = fig.Children;
    
    ax(1).Position(1) = d;
    ax(2).Position(1) = 2*d+w;
    ax(3).Position(1) = 3*d+2*w;
    
    ax(1).Position(2) = y;
    ax(2).Position(2) = y;
    ax(3).Position(2) = y;
    
    ax(1).Position(3) = w;
    ax(2).Position(3) = w;
    ax(3).Position(3) = w;
    
    ax(1).Position(4) = h;
    ax(2).Position(4) = h;
    ax(3).Position(4) = h;
    
    figstruct = getframe(fig);
    close(fig);
    X = figstruct.cdata;
    
    subplot(N,1,i); imshow(X);
    if i == 1
        title('Aligned Subject CT (gray) with atlas VOIs (green)');
    end
    
end

sapp_add_to_qc_pic(subject_dir,ff)
close(ff);

end
