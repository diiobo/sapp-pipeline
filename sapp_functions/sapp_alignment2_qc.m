function sapp_alignment2_qc(subject_dir, ct_image, voi_image, species)

% Define values related to positioning of the slices
d = 0.025;
y = 0.1;
h = 1.1;
w = 1.1/3;

% Define the slices (in millimiters) that are shown
xx = [-1 0.5 2];
yy = [-1 0.5 2];
zz = [-1 0.5 2];
% xx = [0];
% yy = [0];
% zz = [0];

% Bounding box to display
%bb = [-75 -75 -75; 75 75 70];          % [loX loY loZ; hiX hiY hiZ]

N = length(xx); % number of slices

ff = figure('Position',[10 10 150 150],'Visible','Off');

for i = 1:N
    
    fig = spm_figure('Create','Graphics','','off');
    
    fig.Position = [796 20 1400 1400/3];
    
    clear global st
    
    k = spm_orthviews('image',ct_image);
    spm_orthviews('AddColouredImage',k,voi_image,[0 1 0]);
%     spm_orthviews('Xhairs','off')
%     spm_orthviews('BB',bb)
    spm_orthviews('reposition',[xx(i) yy(i) zz(i)]);
    spm_orthviews('redraw');
    
    ax = fig.Children;
    
    ax(1).Position(1) = d;
    ax(2).Position(1) = 2*d+w;
    ax(3).Position(1) = 2*w - d;
    
    ax(1).Position(2) = y;
    ax(2).Position(2) = y;
    ax(3).Position(2) = 0;
    
    if species == 'R'
        
        ax(1).Position(3) = w;
        ax(2).Position(3) = w - 0.1;
        ax(3).Position(3) = w - 0.1;
        
        ax(1).Position(4) = h-0.3;
        ax(2).Position(4) = h-0.3;
        ax(3).Position(4) = h;
        
    else
        
        ax(1).Position(3) = w;
        ax(2).Position(3) = w - 0.1;
        ax(3).Position(3) = w - 0.1;
        
        ax(1).Position(4) = h-0.5;
        ax(2).Position(4) = h-0.5;
        ax(3).Position(4) = h;
    end
    
    figstruct = getframe(fig);
    close(fig);
    X = figstruct.cdata;
    
    subplot(N,1,i); imshow(X);
    if i == 1
        title('Aligned Subject CT (gray) with atlas VOIs (green), ZOOM');
    end
    
end

sapp_add_to_qc_pic(subject_dir,ff)
close(ff);

end
