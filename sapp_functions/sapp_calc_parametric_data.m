function parametric_data = sapp_calc_parametric_data(raw_data, subject_info, type)

weight     = subject_info.weight;
dose       = subject_info.dose;  % is in Mbq

if ~isnumeric(weight), weight = str2double(weight); end
if ~isnumeric(dose),   dose = str2double(dose);     end

dose_bq    = dose * 10^6;  %dose in Bequerels, Assuming we want the images in Bq/ml:


% From SAMIT (samit_standardize_uptake)

switch type
    case 'SUV'
        
        factor = weight/dose_bq;  
        
    case 'IDg' % Percentage of injected dose per gram
        
        factor = 100/dose_bq;    
end

parametric_data = raw_data.*factor;
end