function info = load_info_file(file)

info = readtable(file, 'TreatAsEmpty', 'NA');
info.Properties.RowNames = info.Dataset;

end