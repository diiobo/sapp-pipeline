function new_filename = sapp_reslice(target, source, prefix, interp)

images = {target, source};
%prefix = 'rsl_';
%interp = 1; % trilinear

spm_reslice(images, struct('interp',interp, 'mask',0, 'mean', 0, 'which', 1, 'wrap',[0 0 0]', 'prefix',prefix));

new_filename = spm_file(source, 'prefix', prefix);
end