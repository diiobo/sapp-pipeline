function T_norm = sapp_spatial_normalization(ct_ref, ct_head, rigid)


% Co-registration flags
coreg_flags.coreg.estimate.cost_fun = 'ncc';
coreg_flags.coreg.estimate.sep      = [0.4 0.2];
coreg_flags.coreg.estimate.tol      = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
coreg_flags.coreg.estimate.fwhm     = [5 5];

% Estimate transformation
x = spm_coreg(ct_ref, ct_head, coreg_flags.coreg.estimate);

% Write transformed affine to header
T_norm = spm_matrix(x);

Affine = spm_get_space(ct_head); 
new_Affine = T_norm \ Affine;
spm_get_space(ct_head, new_Affine);


% If user selected affine, perform a second affine registration:
if ~rigid
    
    % allow affine transformation
    coreg_flags.coreg.estimate.params   = [0 0 0  0 0 0  1 1 1  0 0 0];
    
    x = spm_coreg(ct_ref, ct_head, coreg_flags.coreg.estimate);
    
    % Write transformed affine to header
    T_norm2 = spm_matrix(x);
    Affine = spm_get_space(ct_head);
    new_Affine = T_norm2 \ Affine;
    spm_get_space(ct_head, new_Affine);
    
    T_norm = T_norm * T_norm2;
end



%% save T_norm

[p, ~, ~] = fileparts(ct_head);
if ~isdir(sprintf('%s\\TR',p)), mkdir(sprintf('%s\\TR',p)); end
save([sprintf('%s\\TR',p),'\\CTtoRefCT_norm.mat'], 'T_norm');


end