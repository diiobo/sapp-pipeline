function log_filename = sapp_initialize_log_file(top_dir, output_dir)

log_filename = sprintf('%s\\log_file.txt',output_dir);

fid = fopen(log_filename, 'a');
fprintf(fid, 'SAPP running data from folder:\n%s\n', top_dir);
fprintf(fid, '%s \n\n', datetime('now') );
fclose(fid);

end