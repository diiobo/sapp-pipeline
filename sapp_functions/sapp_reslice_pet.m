function sapp_reslice_pet(pet_file, vois_file, prefix, interp)


[bb, ~] = spm_get_bbox(vois_file);

V = spm_vol(pet_file);

new_bb        = bb; % bounding box of VOIs file
new_voxsiz    = [0.2 0.2 0.2];

VV(1)   = V(1);
VV(2:52)= V;
VV(1).mat = spm_matrix([new_bb(1,:) 0 0 0 new_voxsiz])*spm_matrix([-1 -1 -1]);
VV(1).dim = ceil(VV(1).mat \ [new_bb(2,:) 1]' - 0.1)';
VV(1).dim = VV(1).dim(1:3);

% trilinear interpolation
spm_reslice(VV,struct('mean',false,'which',1,'interp', 1, 'prefix', '02_'));


end