function sapp_com_qc(subject_dir, ref_ct_image, ct_image)
% display the ref CT and overlay on top the CT head with the
% origin at COM to see how far away they are before the alignment! 

% Define values related to positioning of the slices
d = 0.05;
w = 0.8/3;
h = 0.8;
y = 0.1;

N = 1; % number of slices

% Define the slices (in millimiters) that are shown
xx = [0];  % At origin of refCT (which is the origin of the atlas!)
yy = [0];
zz = [0];

ff = figure('Position',[10 10 200 200],'Visible','Off');

for i = 1:N
    
    fig = spm_figure('Create','Graphics','','off');
    fig.Position = [796 20 1200 400];
    
    clear global st
    
    k = spm_orthviews('image',ref_ct_image);
    spm_orthviews('AddColouredImage',k ,ct_image,[1 0 0]);
%     spm_orthviews('Xhairs','off')
    spm_orthviews('reposition',[xx(i) yy(i) zz(i)]);
    spm_orthviews('redraw');
    
    ax = fig.Children;
    
    ax(1).Position(1) = d;
    ax(2).Position(1) = 2*d+w;
    ax(3).Position(1) = 3*d+2*w;
    
    ax(1).Position(2) = y;
    ax(2).Position(2) = y;
    ax(3).Position(2) = y;
    
    ax(1).Position(3) = w;
    ax(2).Position(3) = w;
    ax(3).Position(3) = w;
    
    ax(1).Position(4) = h;
    ax(2).Position(4) = h;
    ax(3).Position(4) = h;
    
    figstruct = getframe(fig);
    close(fig);
    X = figstruct.cdata;
    
    subplot(N,1,i); imshow(X);
    title('Estimated initial alignment between Subject CT (gray) and Reference CT (red)');
    
end

sapp_add_to_qc_pic(subject_dir,ff)
close(ff);

end