function frames_long = convert_tframes(frames_short)
%% Convert from frames_short to frames_long

%End points!

frames_long = ones(sum(frames_short(:,1),1) , 1);
x=0;

for i=1:size(frames_short,1)
    frames_long( x+1 : x+frames_short(i,1) ) = frames_long( x+1:x+frames_short(i,1)) .* frames_short(i,2);
    x = x + frames_short(i,1);
end

for j=2:length(frames_long)
    frames_long(j) = frames_long(j) + frames_long(j-1);
end

end

