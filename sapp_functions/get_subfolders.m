function sub_dir_names = get_subfolders(top_dir)

files = dir(top_dir);
dirFlags = [files.isdir]; % Get a logical vector that tells which is a directory.
dirFlags(1:2) = 0; 
sub_dirs = files(dirFlags);  % Extract only those that are directories.

sub_dir_names = {sub_dirs(:).name};

end