function [frames_short, dyn] = sapp_get_frames_dcm2(dcm_dir)

% Reads the first file header, gets number of slices and number of time frames,
% then reads headers from the first slice of each timeframe.

f = get_filenames(dcm_dir,'*.dcm');  % assumes they are sorted!
I = dicominfo(f{1});

nslices = I.NumberOfSlices;

if isfield(I, 'NumberOfTimeSlices')
    % PET is dynamic
    nframes = I.NumberOfTimeSlices;
    
    X = zeros(nframes,2);
    
    k=1;
    for i = 1:nframes
        I = dicominfo(f{k});
        X(i,1) = I.FrameReferenceTime;
        X(i,2) = I.ActualFrameDuration;
        k = k + nslices;
    end
    
    
    X = X/1000; % convert milliseconds to seconds
    X = X/60; % to minutes
    
    [uvals, ~, uidx] = unique(X(:,2));
    frames_short = [accumarray(uidx, 1), uvals];
    dyn = 'dynamic';

elseif ~isfield(I, 'NumberOfTimeSlices') && nslices == length(f)
    %PET is static
    x = I.ActualFrameDuration /1000; % seconds
    x = x/60; % minutes
    frames_short = [1 x];
    dyn = 'static';

else
    fprintf('Error: error in sapp_get_frames_dcm2.m (something might have changed in the dcm header!)')
    
end


end