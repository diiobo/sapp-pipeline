function subjects_info = sapp_get_subjects_info (datasets, top_dir, output_dir)
% Gets the information about the subjects from the Info.xlsx file and
% stores it in a struct
% Creates output folder for each subject


% Retrieve info from Info.xlsx
info_file  = sprintf('%s\\Info.xlsx', top_dir);
info       = load_info_file(info_file);   % Change this! 

if height(info) ~= length(datasets)
    error('The number of datasets specified in the Info.xlsx file does not match the number of folders detected.')
end

% Initialize subjects_info
subjects_info.name       = {};
subjects_info.weight     = []; 
subjects_info.dose       = [];
subjects_info.path       = {};
subjects_info.dataset    = {};
subjects_info.dyn        = [];
subjects_info.tframes    = [];


count =1;
for i=1:length(datasets)
    
    data_set = datasets{i};
            
    %Initialize Output directory for each subject and store its info in subjects_info struct
    
    % if ~isempty(info{data_set,'RightSubject'}{1,1}) && ~isempty(info{data_set,'LeftSubject'}{1,1})
    if ~isempty(info{data_set,'RightSubject'}{1,1}) && ~strcmp(info{data_set,'LeftSubject'}{1,1}, '-')
        
        right_dir = sprintf('%s\\%s', output_dir, char(info{data_set,'RightSubject'}));
        if ~isdir(right_dir), mkdir(right_dir); end

        left_dir = sprintf('%s\\%s', output_dir, char(info{data_set,'LeftSubject'}));
        if ~isdir(left_dir), mkdir(left_dir); end

        
        subjects_info(count).name         = char(info{data_set,'RightSubject'});
        subjects_info(count).weight       = info{data_set,'RightWeight'};
        subjects_info(count).dose         = info{data_set,'RightIDose'};
        subjects_info(count).path         = right_dir;
        subjects_info(count).dataset      = data_set;
        [subjects_info(count).tframes,...
            subjects_info(count).dyn]     = get_dataset_tframe_info(data_set, top_dir);

        count = count +1;        
        
        subjects_info(count).name         = char(info{data_set,'LeftSubject'});
        subjects_info(count).weight       = info{data_set,'LeftWeight'};
        subjects_info(count).dose         = info{data_set,'LeftIDose'};
        subjects_info(count).path         = left_dir;
        subjects_info(count).dataset      = data_set;
        subjects_info(count).tframes      = subjects_info(count-1).tframes;   %same dataset!
        subjects_info(count).dyn          = subjects_info(count-1).dyn;       %same dataset!

        count = count +1;        



    elseif ~isempty(info{data_set,'RightSubject'}{1,1})
        
        right_dir = sprintf('%s\\%s', output_dir, char(info{data_set,'RightSubject'}));
        if ~isdir(right_dir), mkdir(right_dir); end
        
        subjects_info(count).name      = char(info{data_set,'RightSubject'});
        subjects_info(count).weight    = info{data_set,'RightWeight'};
        subjects_info(count).dose      = info{data_set,'RightIDose'};
        subjects_info(count).path      = right_dir;
        subjects_info(count).dataset   = data_set;
        [subjects_info(count).tframes,...
            subjects_info(count).dyn]     = get_dataset_tframe_info(data_set, top_dir);

        count = count +1;     
        
        
    else 
        error('Subject name and information for dataset %s are not correctly set in the info file',data_set)
    end

    
end