function T_com = sapp_cg_set_com_mod(imagefile, species)
% use center-of-mass (COM) to roughly correct for differences in the
% position between image and template
% does not actually change the origo, but changes the affine transformation
% so that after the translation the

%FROM TOMI

% pre-estimated COM of atlas template
if species == 'M'
    com_reference = [0 0 -3.5];   
else
    com_reference = [0 -5 -5];  % (0, left/right, up/down) from COM
%     com_reference = [0 -3.5 -4];
end

V = spm_vol(imagefile);

%fprintf('Correct center-of-mass for %s\n',V.fname);
T_com = eye(4);
vol = spm_read_vols(V);
avg = mean(vol(:));
avg = mean(vol(vol>avg));

% don't use background values
[x,y,z] = ind2sub(size(vol),find(vol>avg));
com = V.mat(1:3,:)*[mean(x) mean(y) mean(z) 1]';
com = com';

M = spm_get_space(V.fname);
T_com(1:3,4) = (com - com_reference)';
W = T_com \ M;


[p, ~, ~] = fileparts(imagefile);
if ~isdir(sprintf('%s\\TR',p)), mkdir(sprintf('%s\\TR',p)); end
save([sprintf('%s\\TR',p),'\\CT_com.mat'], 'T_com');

%V.descrip = 'centered';
spm_get_space(V.fname,W);

end