function nii_fnames = spm_dcm2nii(dicom_path,nii_path)

f = spm_select('FPList',dicom_path,'.dcm');
if(isempty(f))
    f = spm_select('FPList',dicom_path,'.*');
end
% t = 0.2930; % seconds per header
% N = size(f,1);
% T = round(t*N/60,1);
% fprintf('Reading headers of %.0f dicom files... This will take ~ %.1f minutes\n',size(f,1),T);
fprintf('Reading headers of %.0f dicom files...\n',size(f,1));
hdr_list = spm_dicom_headers(f);
fprintf('Converting the dicoms into nifti format...\n');
nii_fnames = spm_dicom_convert(hdr_list,'all','flat','nii',nii_path);

end