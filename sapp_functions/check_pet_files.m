function check = check_pet_files(dataset, top_dir)
% Check if the PET is missing any DICOM files by calculating how many files
% there should be
%
% check = 1 if all ok and 0 if there are files missing


pet_dir = sprintf('%s\\%s\\PT_%s',top_dir, dataset, dataset);

f = get_filenames(pet_dir,'*.dcm');

% read metadata of first dcm file
I = dicominfo(f{1});


if isfield(I, 'NumberOfTimeSlices')      % if PET is dynamic
    nframes = I.NumberOfTimeSlices;
    nslices = I.NumberOfSlices;
    
else   % PET static
    nslices = I.NumberOfSlices;
    nframes = 1;  
end

check = 1;
if numel(f) < (nslices * nframes) 
    check = 0;
end



end