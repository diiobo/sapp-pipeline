function M = calculate_voi_analysis(pet_file, Species)
 % needs Species structure

sapp_def = sapp_defaults(Species);
voi_file = sapp_def.voi;
clear sapp_def

voi_V = spm_vol(voi_file);

%load voi_vol
voi_vol = logical(spm_read_vols(voi_V));

%load pet headers
pet_V = spm_vol(pet_file);


%% Initialize variables

nfiles = numel(pet_V);      % Number of files to analyse = frames
nvois = numel(voi_V);       % Number of regions

M = zeros(nfiles, nvois);  %Results matrix of size (t_frames x VOIs)


%% Evaluation of the VOIs
% code from Samit, modified

for f = 1:nfiles
    
    current_pet_V = pet_V(f);
    
    for i=1:nvois
        
        current_voi_V = voi_V(i);
        current_voi_Mask = voi_vol(:,:,:,i);
        
        % Construct vector of the region
        idx = find( current_voi_Mask == 1);
        plane   = current_voi_V.dim(1)*current_voi_V.dim(2);
        voi_x   =      mod(idx, current_voi_V.dim(1));
        voi_y   =  fix(mod(idx, plane ) / current_voi_V.dim(1)) +1;
        voi_z   =  fix(    idx/ plane ) +1;
        
        % MASK voxel coordinate of the VOI
        XYZ = [ voi_x, voi_y, voi_z, ones(length(idx), 1) ]';
        
        
        data = spm_data_read(current_pet_V, 'xyz', XYZ);
        
        M(f,i) = mean(data);
        
        
        
    end
end

end