function sapp_calculate_parametric_images(pet_file, subject_info, params)

type    = params.parametricImageType;
refROI  = params.parametricRefROI;
Species = params.Species;

if type == 1
    type = 'SUV';
elseif type == 2
    type = 'IDg';
end

weight     = subject_info.weight;
dose       = subject_info.dose;  % is in Mbq

if ~isnumeric(weight), weight = str2double(weight); end
if ~isnumeric(dose),   dose = str2double(dose);     end

dose_bq    = dose * 10^6;  %dose in Bequerels, Assuming we have the raw images in Bq/ml:


[p,n,e] = fileparts(pet_file);
filename = sprintf('%s\\%s_%s%s',p, n, type,e );



% From SAMIT (samit_standardize_uptake)

switch type
    case 'SUV'
        
        factor = weight/dose_bq;   % Formula
        descrip = ['SUV: Dose ' num2str(dose_bq) 'Bq; Weight ' num2str(weight) 'gr'];
        
    %case 'SUVcorr'  % SUV corrected for the mean uptake of the whole brain
        
        
        
    case 'IDg' % Percentage of injected dose per gram
        
        factor = 100/dose_bq;   % Formula
        descrip = ['%ID/g: Dose ' num2str(dose_bq) 'Bq; Weight ' num2str(weight) 'gr' ];
        
    otherwise
        
        fprintf('Operation cancelled: Unexpected normalization type.');
        return
end



% If reference ROI was selected, compute average signal inside ROI
if refROI
    M = calculate_voi_analysis(pet_file, Species);
    mean_refROI = M(:, refROI);  % size = length(timeFrames) x 1 refROI
end

%% Calculate and write new SUV image

V = spm_vol(pet_file);
vol = spm_read_vols(V);

frames = cell(numel(V),1);
for i=1:numel(V)
    
    % Compute the parametric volume for timeframe i
    if exist('mean_refROI', 'var')
        parametric_vol = vol(:,:,:,i)./ mean_refROI(i);
    else
        parametric_vol = vol(:,:,:,i).* factor;
    end
        

    % metadata
    Vo = V(i);
    Vo.fname = spm_file(pet_file, 'suffix', ['_', type, '_', num2str(i)]);
    Vo = rmfield(Vo, 'pinfo');    % if no pinfo field, spm will calculate it!
    Vo = rmfield(Vo, 'n'); % this was giving trouble, vol with n, it always stored a 4d file with empty frames before n
    Vo.descrip = descrip;
    Vo.dt = [8 0];   % dtype: 4= int16, 8=int32! using int16 gave some problems when saving ratio images! 
    
    spm_write_vol(Vo, parametric_vol);
    
    frames{i} = Vo.fname;
end

spm_nifti_dynamize(frames , filename);

% spm_nifti_dynamize doesn't delete the file if it's static
if length(frames) == 1
    delete(char(frames))
end

% If it exists:
mat_file = sprintf('%s\\%s_%s.mat',p, n, type );
delete(mat_file);

end


