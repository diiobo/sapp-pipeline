function [out_fname]= crop_head(img_file, crop_coord, varargin)

% crop_coord = [X1 X2
%               Y1 Y2
%               Z1 Z2]

if (nargin == 3)
    out_fname = varargin{1};
else
    [p,n,~] = fileparts(img_file);
    out_fname = sprintf('%s\\%s_head.nii',p,n);
end


V = spm_vol(img_file);



% Checkup that coordinates are not out of bound
if crop_coord(1,1) < 1,            crop_coord(1,1) = 1;             end
if crop_coord(1,2) > V(1).dim(1),  crop_coord(1,2) = V(1).dim(1);   end
if crop_coord(2,1) < 1,            crop_coord(2,1) = 1;             end
if crop_coord(2,2) > V(1).dim(2),  crop_coord(2,2) = V(1).dim(2);   end
if crop_coord(3,1) < 1,            crop_coord(3,1) = 1;             end
if crop_coord(3,2) > V(1).dim(3),  crop_coord(3,2) = V(1).dim(3);   end




if length(V) == 1  % file is 3D when CT
    img_vol = V.private.dat(:,:,:);
    head_img_vol = img_vol(crop_coord(1,1):crop_coord(1,2), ...
                           crop_coord(2,1):crop_coord(2,2), ...
                           crop_coord(3,1):crop_coord(3,2));
    
else        % file is 4D when PET
    img_vol = spm_read_vols(V);
    head_img_vol = img_vol(crop_coord(1,1):crop_coord(1,2), ...
                           crop_coord(2,1):crop_coord(2,2), ...
                           crop_coord(3,1):crop_coord(3,2), :);
end


% calc new affine matrix
[x,y,z,t] = size(head_img_vol);
newV.mat = V(1).mat;
newV.mat(1,4) = - newV.mat(1,1) * floor(x /2);
newV.mat(2,4) = - newV.mat(2,2) * floor(y /2);
newV.mat(3,4) = - newV.mat(3,3) * floor(z /2);


% save file
for i=1:t
    %fprintf('%d \n',i);
    img = head_img_vol(:,:,:,i);
    V(i).fname = out_fname;
    V(i).dim = size(img);
    V(i).mat = newV.mat;
    V(i).descrip = strcat(V(i).descrip,' cropped');
    spm_write_vol(V(i),img);
end

% head_file = out_fname;
end