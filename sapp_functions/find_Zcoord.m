function [Zcoord] = find_Zcoord(ct_vol)

% !TO DO!:
%calculate the size of radius i'm looking for based in img pixel size
%and physical size of the anestesia mask

% V = spm_vol(ct_file);
% ct_vol = V.private.dat(:,:,:);

thres = -500;

cor_vol = flipud(permute(ct_vol, [2 1 3])); 
[~, ~, nZ] = size(cor_vol);

bw_vol = imgaussfilt(cor_vol(:,:,1:floor(nZ/4)),2) > thres; % search only in the first 1/4 of the volume 

% detect circles
circles(1:size(bw_vol,3)) = struct('center', [], 'radius', []);

for i=1:size(bw_vol,3)
[centers, det_radii] = imfindcircles(bw_vol(:,:,i),[40 80], 'Sensitivity', 0.85);  %default 0.85, circularity???
    if length(det_radii) > 1     %choose biggest circle: radii, center
       [radius,loc] = max(det_radii);
       center = centers(loc, :);
    elseif length(det_radii) == 1
       radius = det_radii;
       center = centers;
    else
       radius = 0; center = 0;
    end
circles(i).center = center;
circles(i).radius = radius;
end


Radii = [circles.radius];
pos_imgs = find(Radii); % positive images: a circle (or more) was detected

if ~isempty(pos_imgs)
    if length(pos_imgs) > 1 
        max_vals = find(diff(pos_imgs) >= 20);
        if isempty(max_vals)
            Zcoord = max(pos_imgs);
        else 
            Zcoord = pos_imgs(max_vals(1));
        end
    else
        Zcoord = pos_imgs;
    end
else
    warning('No circle was detected - detecting anesthesia mask: the head is cropped from Zcoord = 1');
    Zcoord = 1;
end
        
        
      
end


