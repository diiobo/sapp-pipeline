function sapp_head_reorient(subject_info)
% Reorients both the CT and PET to the atlas orientation
% Also saves backup files of the extracted+reoriented heads

subject_dir  = subject_info.path;

ct_head = sprintf('%s\\CT_%s_head.nii',subject_dir, subject_info.name);
pet_head = sprintf('%s\\PT_%s_head.nii',subject_dir,subject_info.name);


%%  Reorient both PET and CT to atlas orientation

sapp_reorient_file(ct_head);
sapp_reorient_file(pet_head);


% Save backup in a different folder
mkdir(sprintf('%s\\Head_original',subject_dir));
copyfile(ct_head, sprintf('%s\\Head_original',subject_dir));
copyfile(pet_head, sprintf('%s\\Head_original',subject_dir));




end
