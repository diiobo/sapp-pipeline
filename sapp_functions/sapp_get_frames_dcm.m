function [frames_short, dyn] = sapp_get_frames_dcm(dcm_dir)

%fprintf('Reading start times and durations of frames from dicom headers...');

f = get_filenames(dcm_dir,'*.dcm');
X = zeros(length(f),2);


for i = 1:length(f)
    I = dicominfo(f{i});
    X(i,1) = I.FrameReferenceTime;
    X(i,2) = I.ActualFrameDuration;
end

Y = unique(X,'rows');

Y = Y/1000; % convert milliseconds to seconds
Y = Y/60; % convert seconds to minutes

if size(Y,1) == 1
    % PET is static
    dyn = 'static';
    x = I.ActualFrameDuration /1000; % seconds
    x = x/60; % minutes
    frames_short = [1 x];
    
else
    % PET is dynamic
    dyn = 'dynamic';
    [uvals, ~, uidx] = unique(Y(:,2));
    frames_short = [accumarray(uidx, 1), uvals];
    
end

end