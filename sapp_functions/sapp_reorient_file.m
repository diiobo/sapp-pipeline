function sapp_reorient_file(filename)
% Performs a pitch rotation + a flip across the Y-axis (in SPM terms)
% Modified from SAMIT toolbox (samit_reorient)

frames = Inf;

[p,n,e] = fileparts(filename);  % path, name, extention
files = cellstr(spm_select('ExtFPList',p,[n e], frames));


% From SAMIT (samit_reorient):
for i = 1:numel(files)
    
    V    = spm_vol(files{i});
    vol    = spm_read_vols(V);
    M    = V.mat;
    vox  = sqrt(sum(M(1:3,1:3).^2));
    
    Orig = abs([(M(1,4)/M(1,1)) (M(2,4)/M(2,2)) (M(3,4)/M(3,3))]);   %Orig = V.mat\[0 0 0 1]';
    
    % df: difference between center and origin coordinates
    df = Orig - ((V.dim+1)./2);
    dfm = df;
    dfm(2) = -df(3);
    dfm(3) = df(2);
    
    dfm = spm_matrix(dfm .* vox);
    
    %     % Rotation matrix: pitch rotation
    %     theta = -pi/2;
    %     X = [cos(theta) 0       -sin(theta)        0;
    %         0           1                0         0;
    %         sin(theta)  0       cos(theta)         0;
    %         0           0                0         1];
    %
    %     tform = affine3d(X);
    %     RY = imref3d(size(Y),vox(1),vox(2),vox(3)); % Limits for rotation
    %     Y = imwarp(Y,RY,tform);
    
    % Same rotation can be done with:
    new_vol = rot90(permute(vol, [3 1 2]),3);
%     new_vol = flip(rot90(permute(vol, [3 1 2]),1),3); % for aligning Shiffer atlas to Samit orientation!

    % Correct location of origin coordinates
    V.dim = size(new_vol);
    
    %correct voxel size (check if ok)
    vox = [vox(1), vox(3), vox(2)];
    
    cntr = spm_matrix(vox/2) \ spm_matrix([-(V.dim .* vox /2) 0 0 0 vox]);
    W = dfm \ cntr;
    
    %% Write new file
    V.mat = W;
    V.private.mat = W;
    V.private.mat0 = W;
    V.fname = spm_file(V.fname,'prefix','r_');
    spm_write_vol(V, new_vol);

end

movefile(V.fname,filename,'f');

end