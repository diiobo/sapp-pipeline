function sapp_convert_dataset_to_nifti(data_set, top_dir)
% Check if nifti files of the CT and PET exist and if not, convert Dicom to Nifti

data_dir = sprintf('%s\\%s',top_dir,data_set);

ct_dir = sprintf('%s\\CT_%s',data_dir, data_set);
pet_dir = sprintf('%s\\PT_%s',data_dir, data_set);


%% CT
% check if nifti file exists (YES: ct_file  NO: convert dcm2nii)

ct_file = sprintf('%s.nii',ct_dir); % one 3D NIFTI file

if(~exist(ct_file,'file'))
    if(exist(ct_dir,'dir'))
        sapp_convert_to_nifti(ct_dir, data_dir, ct_file);
    else
        error('Could not find raw CT data (dicom) from %s.',ct_dir);
    end
end


%% PET
% check if nifti file exists (YES: pet_file  NO: convert dcm2nii)

pet_file = sprintf('%s.nii',pet_dir); % 4D NIFTI is PET is dynamic

if(~exist(pet_file,'file'))
    if(exist(pet_dir,'dir'))
        sapp_convert_to_nifti(pet_dir, data_dir, pet_file);
    else
        error('Could not find raw PET data (dicom) from %s.',pet_dir);
    end
end


end


