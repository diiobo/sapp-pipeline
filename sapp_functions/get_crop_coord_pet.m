function [crop_coord_pet] = get_crop_coord_pet(ct_file, pet_file, crop_coord_ct)

first_corner_ctVoxel = [crop_coord_ct(1,1) crop_coord_ct(2,1) crop_coord_ct(3,1) 1];
last_corner_ctVoxel  = [crop_coord_ct(1,2) crop_coord_ct(2,2) crop_coord_ct(3,2) 1];

V_ct = spm_vol(ct_file);
first_corner_World   = V_ct.mat * first_corner_ctVoxel';
last_corner_World    = V_ct.mat * last_corner_ctVoxel';

V_pet = spm_vol(pet_file);
first_corner_petVoxel = V_pet(1).mat \ first_corner_World;
last_corner_petVoxel  = V_pet(1).mat \ last_corner_World;

crop_coord_pet = [floor(first_corner_petVoxel(1)) ceil(last_corner_petVoxel(1));...
                  floor(first_corner_petVoxel(2)) ceil(last_corner_petVoxel(2));...
                  floor(first_corner_petVoxel(3)) ceil(last_corner_petVoxel(3))];

end
