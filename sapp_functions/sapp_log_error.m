function sapp_log_error(ME, filename, varargin)
% Usage: sapp_log_error(ME, filename)
%        sapp_log_error(ME, filename, 'dataset', data_set)
%        sapp_log_error(ME, filename, 'subject', subject)


% Open log file
fid = fopen(filename, 'a');

if (nargin == 2)
    
    fprintf(fid, '\n\nError message\n-------------\n\n');
    fprintf(fid, '%s\n', ME.getReport('extended', 'hyperlinks','off'));
    
elseif (nargin == 4) && strcmp(varargin{1}, 'dataset')
    
    data_set = varargin{2};
    fprintf(fid, '\n\nWARNING!\n-------------\n\n');
    fprintf(fid, 'Something went wrong when processing DATASET %s. SAPP will skip it!', data_set);
    fprintf(fid, '\n\nError message dataset %s\n-------------\n\n', data_set);
    fprintf(fid, '%s', ME.getReport('extended', 'hyperlinks','off'));

elseif (nargin == 4) && strcmp(varargin{1}, 'subject')
    
    subject = varargin{2};
    fprintf(fid, '\n\nWARNING!\n-------------\n\n');
    fprintf(fid, 'Something went wrong when processing SUBJECT %s. SAPP will skip it!', subject);
    fprintf(fid, '\n\nError message subject %s\n-------------\n\n', subject);
    fprintf(fid, '%s', ME.getReport('extended', 'hyperlinks','off'));

else 
    error('Incorrect input arguments to function sapp_log_error')
end

% Close log file    
fclose(fid);

end