function sapp_ps2pdf(subject_dir)

[path, subject,~] = fileparts(subject_dir);

% init for ps2pdf
ps_file = sprintf('%s/%s/qc_%s.ps', path, subject, subject);
pdfpath = sprintf('%s/%s/qc_%s.pdf', path, subject, subject);

gs      = 'C:\DianaIB\SOFTWARE\gs9.22\bin\gswin64c.exe';
gslib   = 'C:\DianaIB\SOFTWARE\gs9.22\lib';

%'gsfontpath', 'C:\Program Files\GhostScript\fonts', ...


ps2pdf('psfile', ps_file, 'pdffile', pdfpath, 'gscommand', gs, 'gslibpath', gslib);

delete(ps_file);

end