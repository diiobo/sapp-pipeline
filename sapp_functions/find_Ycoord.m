function [Ycoord, lateral_Xcoords] = find_Ycoord(ct_file)
% Detects where the bed is in the saggital images and computes the Y coord
% based on it
% also returns laterla_Xcoords = detected bed limits in coronal view 

% TO DO: QC, return a figure of the mid coronal plane with the detected bed curve

V       = spm_vol(ct_file);
ct_vol  = V.private.dat(:,:,:);

%% get coronal image for plotting later
% cor_vol = flipud(permute(ct_vol, [2 1 3]));
% mid_coronal = cor_vol(:,:,round(size(cor_vol,3)/2));
% clear cor_vol;


%% reformat data in SAGITTAL view
ct_vol = fliplr(permute(ct_vol, [3 2 1]));  

[nX, nY, nZ] = size(ct_vol);
thres = -500;
h = [1 0 -1
     2 0 -2
     1 0 -1];  % filter for edge detection

bed_thickness = 10;  % pixels

bw_vol = imgaussfilt(ct_vol,2) > thres;

lines(1:nZ) = struct('point1',0,'point2',0,'theta',0,'rho',0);

for i = 1:nZ
   % fprintf('%d \n',i)
    img_edges = imfilter(bw_vol(:,:,i), h);  % get edges from bw_vol
    [H,T,R] = hough(img_edges, 'Theta', -1:0.1:1);
    P  = houghpeaks(H,1);
    detected_lines = houghlines(img_edges,T,R,P,'FillGap',150,'MinLength',floor(nX/2));
    
    if ~isempty(detected_lines)
        if size(detected_lines,2) == 1 % only 1 line detected
            lines(i) = detected_lines;
        else                  % more than one, choose longest line
            max_len = 0;
            for k = 1:length(detected_lines)
                 len = norm(detected_lines(k).point1 - detected_lines(k).point2);
                 if ( len > max_len)
                     max_len = len;
                     lines(i) = detected_lines(k);
                 end
            end
        end   
    end 
end

rho = movmedian([lines.rho],9); %smoothed curve

% get coordinates for head extraction
lateral_Xcoords = [find(rho, 1) find(rho,1,'last')];
% Ycoord = nY - ((max(rho(rho~=0))) - 50);
Ycoord = nY - (max(rho(rho~=0))) ;

% remove bed from ct_vol
% ct_vol_wBed = ct_vol;
% ct_vol_wBed(:,1:Ycoord,:) = -1000;


% % plot it on coronal view
% ff = figure('Visible','Off');
% imagesc(mid_coronal)
% hold on, plot(rho,'r', 'Linewidth', 2);

% TO DO: save figure in the folder of the ct_file
% print('-painters','-fillpage','-r150','-noui',figure,'-dpsc',qc_pic_fname);


end
