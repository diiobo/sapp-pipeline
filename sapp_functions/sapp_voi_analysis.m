function sapp_voi_analysis(pet_file, results_file, specie)
% OLD!!!!!!!!!!!

% pet_file = 'C:\DianaIB\DATA\MOUSE_RESULTS\P100163_R\PT_P100163_R_head.nii';
% specie = 'M';

% load defaults
sapp_def = sapp_defaults(specie);
atlas_file = sapp_def.atlas;

SUV_file = spm_file(pet_file, 'suffix', ['_', 'SUV']);
IDg_file = spm_file(pet_file, 'suffix', ['_', 'IDg']);

M_suv = calculate_voi_analysis(SUV_file, specie);
M_idg = calculate_voi_analysis(IDg_file, specie);

%% Save results

% Load the names for each region in the VOI
voi_txt = spm_file(atlas_file,'ext','.txt');
fid = fopen(voi_txt);
C = textscan(fid, '%d %s', 'Delimiter', '\t', 'CommentStyle', '//');
fclose(fid);
voi_names = [cellstr(C{2})];

xlRange = 'B2';
xlswrite(results_file, M_suv , 'SUV', xlRange);
xlswrite(results_file, M_idg , 'IDg', xlRange);

xlRange = 'A1:V1';
names = ['Time (min)'; voi_names]';
xlswrite(results_file, names , 'SUV', xlRange);
xlswrite(results_file, names , 'IDg', xlRange);


end




