function T_coreg = sapp_estimate_initial_coreg(ref_img, source_img)
% Estimates the initial coregistration of the whole-body PET to CT
% - no reslicing
% - writes the affine matrix to the source_img header

% Co-registration flags
coreg_flags.coreg.estimate.cost_fun = 'nmi';
coreg_flags.coreg.estimate.sep      = 1;
coreg_flags.coreg.estimate.tol      = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
coreg_flags.coreg.estimate.fwhm     = [5 5];


% call directly to spm_coreg function
x = spm_coreg(ref_img, source_img, coreg_flags.coreg.estimate);   
T_coreg = spm_matrix(x);


end

