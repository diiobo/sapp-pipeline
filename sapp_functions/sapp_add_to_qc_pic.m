function sapp_add_to_qc_pic(subject_dir,figure)
% Adds the Matlab figure to the subject's QC file.

% from Tomi K. at Turku PET Centre

[path, subject,~] = fileparts(subject_dir);
qc_pic_fname = sprintf('%s\\%s\\qc_%s.ps',path,subject,subject);


% print('-painters','-fillpage','-r150','-noui',figure,'-dpdf',qc_pic_fname);

if(~exist(qc_pic_fname,'file'))
    print('-painters','-fillpage','-r150','-noui',figure,'-dpsc',qc_pic_fname);
else
    print('-painters','-fillpage','-r150','-noui',figure,'-dpsc','-append',qc_pic_fname);
end
    
end