function [Xcoord] = find_Xcoord(ct_file)
% Checks number of animals in the CT FOV (1 or 2)
%__________________________________________________________________________


V = spm_vol(ct_file);
ct_vol = V.private.dat(:,:,:);

%Coronal sum 
cor_sum = mat2gray(squeeze(sum(ct_vol,2))); 
cor_sum = imgaussfilt(cor_sum , 4);

% Projection along Z-direction
Z_proj = sum(cor_sum,2);

% fit curve
fitresult = createFit(Z_proj); 
x=1:length(Z_proj);
curve = fitresult(x);

% figure,plot(curve)

% find maxima
[pks,~] = findpeaks(curve);

if length(pks) == 1
    Xcoord = 0;
elseif length(pks) ==2
    [~,Xcoord] = findpeaks(-curve);  % put MID point of image better!
else
    error('Number of subjects identified in the CT is not 1 or 2... %s.\n',ct_file);
end


end


%--------------------------------------------------------------------------
%---------------------    createFIT subfunction   ------------------------
%--------------------------------------------------------------------------
% This uses the curve-fitting toolbox!
function [fitresult, gof] = createFit(zproj)

[xData, yData] = prepareCurveData( [], zproj );

% Set up fittype and options.
ft = fittype( 'gauss2' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

end


