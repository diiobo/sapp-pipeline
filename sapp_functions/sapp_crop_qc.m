function ff = sapp_crop_qc(subject_dir, ct_image, pet_image)
% Visualizes orthogonal slices of PET and MR images.
% The visualization is saved to the subject's qc file.

[~,subject,~] = fileparts(subject_dir);

images = {ct_image, pet_image}';

titles ={[subject,' Cropped CT'], 'Cropped PET', 'Fused'}';

% Define values related to positioning of the slices
d = 0.05;
w = 0.8/3;
h = 0.8;
y = 0.1;


% Define the slices (in millimiters) that are shown
xx = 0;    %at origin
yy = 0;
zz = 0;

ff = figure('Position',[10 10 200 200],'Visible','Off');
% ff = figure('Position',[10 10 200 200],'Visible','On');

for i=1:3
    
    fig = spm_figure('Create','Graphics','','off');
%     fig = spm_figure('Create','Graphics','','on');
    
    fig.Position = [796 20 1200 400];
    
    clear global st
    if i==3
        k = spm_orthviews('image',images{1});  %CT
        %spm_orthviews('AddColouredImage', k , images{2}, [0 0 1]);  %PET
        cmap = jet;
        spm_orthviews('addtruecolourimage', k, images{2}, cmap, 0.2);  
        % this works ok because I disabled "addcolourbar" from
        % addtrucolourimage function

    else
        spm_orthviews('image',images{i});
    end
    
%     spm_orthviews('Xhairs','off')
    spm_orthviews('reposition',[xx yy zz]);
    spm_orthviews('redraw');
    
    ax = fig.Children;
    
    ax(1).Position(1) = d;
    ax(2).Position(1) = 2*d+w;
    ax(3).Position(1) = 3*d+2*w;
    
    ax(1).Position(2) = y;
    ax(2).Position(2) = y;
    ax(3).Position(2) = y;
    
    ax(1).Position(3) = w;
    ax(2).Position(3) = w;
    ax(3).Position(3) = w;
    
    ax(1).Position(4) = h;
    ax(2).Position(4) = h;
    ax(3).Position(4) = h;
    
    figstruct = getframe(fig);
    close(fig);
    X = figstruct.cdata;
    
    subplot(3,1,i); imshow(X);
    title(titles{i});
end

sapp_add_to_qc_pic(subject_dir,ff)
close(ff);

end

