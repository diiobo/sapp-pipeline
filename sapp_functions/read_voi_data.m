function voi_data = read_voi_data(Species)

sapp_def = sapp_defaults(Species);
atlas_file = sapp_def.atlas;
clear sapp_def

% Load VOI names from text file
voi_txt = spm_file(atlas_file,'ext','.txt');
fid = fopen(voi_txt);
voi_data = textscan(fid, '%d %s', 'Delimiter', '\t', 'CommentStyle', '//');
fclose(fid);
% voi_names = cellstr(voi_data{2});

end