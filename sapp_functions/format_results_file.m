function format_results_file(file)

Excel = actxserver('Excel.Application');
wb = Excel.Workbooks.Open(file);

n_sheets = wb.sheets.Count;

wb.Worksheets.Item(1).Name = 'Info'; % # rename 1st sheet

for i=2:n_sheets
    
    wsheet = wb.Sheets.Item(i);
    wsheet.Activate();
    wsheet.Range('A1').EntireRow.Font.Bold=1;
    wsheet.Application.ActiveWindow.ScrollRow = 1;
    wsheet.Application.ActiveWindow.SplitRow = 1;
    wsheet.Application.ActiveWindow.FreezePanes = true; 
    
end

ffile = spm_file(file, 'suffix', ['_', 'f']);

SaveAs(wb, ffile)
wb.Saved = 1;
    
Close(wb)
Quit(Excel)
delete(Excel)

copyfile(ffile, file);
delete(ffile);
end