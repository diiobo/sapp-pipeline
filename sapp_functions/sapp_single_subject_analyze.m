function sapp_single_subject_analyze(subject_info, params)

refROIs   = params.refROIs; 
avgFrames = params.avgFrames;
    

%resliced pet file
pet_file     = sprintf('%s\\rs02_PT_%s_head.nii', subject_info.path, subject_info.name);  
%pet_file     = sprintf('%s\\Resliced_rs02_PT_%s_head.nii', subject_info.path, subject_info.name);  



%% ROI Analysis
%  raw data (Bq/ml) is measured from the resliced PET
%  SUV and IDg values are derived from the raw data

raw_data = calculate_voi_analysis(pet_file, params.Species);

suv_data = sapp_calc_parametric_data(raw_data, subject_info, 'SUV');
idg_data = sapp_calc_parametric_data(raw_data, subject_info, 'IDg');


%% Calculate ratios with reference regions

if refROIs   
    ratios = calculate_ratios(raw_data, refROIs);
end


%% Average frames if requested by user

if ~isnan(avgFrames(1)) && strcmp(subject_info.dyn, 'dynamic')
    
    raw_avg   = calculate_average_data(raw_data, avgFrames(1), avgFrames(2));
    suv_avg   = calculate_average_data(suv_data, avgFrames(1), avgFrames(2));
    idg_avg   = calculate_average_data(idg_data, avgFrames(1), avgFrames(2));
    
    if refROIs
        ratios_avg = calculate_average_data(ratios, avgFrames(1), avgFrames(2));
    end
end

%% Load VOI names from text file

voi_data = read_voi_data(params.Species);
voi_names = cellstr(voi_data{2}); 


%% Get time-frame information from the subject_info.tframes

tframes = convert_tframes(subject_info.tframes);

names = ['Time (min)'; voi_names]';


%% Save results
%results file name
results_file = sprintf('%s\\%s_results.xlsx', subject_info.path, subject_info.name);

warning('off','MATLAB:xlswrite:AddSheet')  % disable new worksheet warning

% Write info page:
info = {subject_info.name, subject_info.dataset, subject_info.weight, subject_info.dose, subject_info.dyn}';
info_names = {'Subject', 'Dataset', 'Weight (g)', 'Injected dose', 'Dynamic'}';
xlswrite(results_file, info_names);
xlswrite(results_file, info, 'B1:B5');


% Write raw data:
xlswrite(results_file, names , 'Bq_ml');
xlswrite(results_file, tframes, 'Bq_ml', 'A2');
xlswrite(results_file, raw_data, 'Bq_ml', 'B2');
if exist ('raw_avg', 'var')
    xlswrite(results_file, {['Avg ', num2str(tframes(avgFrames(1)-1)), '-', num2str(tframes(avgFrames(2)))]} , 'Bq_ml', ['A', num2str(length(tframes) + 4)]);
    xlswrite(results_file, raw_avg , 'Bq_ml', ['B', num2str(length(tframes) + 4)]); 

end

% Write SUV data:
xlswrite(results_file, names , 'SUV');
xlswrite(results_file, tframes, 'SUV', 'A2');
xlswrite(results_file, suv_data, 'SUV', 'B2');
if exist ('suv_avg', 'var')
    xlswrite(results_file, {['Avg ', num2str(tframes(avgFrames(1)-1)), '-', num2str(tframes(avgFrames(2)))]} , 'SUV', ['A', num2str(length(tframes) + 4)]);
    xlswrite(results_file, suv_avg , 'SUV', ['B', num2str(length(tframes) + 4)]); 
end

% Write IDg data:
xlswrite(results_file, names , 'IDg');
xlswrite(results_file, tframes, 'IDg', 'A2');
xlswrite(results_file, idg_data, 'IDg', 'B2');
if exist ('idg_avg', 'var')
    xlswrite(results_file, {['Avg ', num2str(tframes(avgFrames(1)-1)), '-', num2str(tframes(avgFrames(2)))]} , 'IDg', ['A', num2str(length(tframes) + 4)]);
    xlswrite(results_file, idg_avg , 'IDg', ['B', num2str(length(tframes) + 4)]);
end

% Write ratios data:
if refROIs  
    for i=1:length(refROIs)
        sheet = sprintf('Ratio_%s', voi_names{refROIs(i)});
        xlswrite(results_file, names , sheet); 
        xlswrite(results_file, tframes , sheet, 'A2');
        xlswrite(results_file, ratios(:,:,i), sheet , 'B2');
        if exist ('suv_ratios_avg', 'var')
            xlswrite(results_file, {['Avg ', num2str(tframes(avgFrames(1)-1)), '-', num2str(tframes(avgFrames(2)))]} , sheet, ['A', num2str(length(tframes) + 4)]);
            xlswrite(results_file, ratios_avg(:,:,i) , sheet, ['B', num2str(length(tframes) + 4)]);
        end
    end
end


%% FORMAT the Results file

format_results_file(results_file);




