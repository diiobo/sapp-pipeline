function Vo = mean_pet(filename)

Vi = spm_vol(filename);
Vo = spm_file(filename, 'prefix','Mean_');

f= 'mean(X)';
flags = {1, 0, 1, 4};
Vout = spm_imcalc(Vi, Vo, f, flags);

end