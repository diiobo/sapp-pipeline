function sapp_main(params)
% SAPP_MAIN  Main function for Small Animal PET/CT Pipeline (SAPP)
%
% Written at the Preclinical Imaging laboratory, Turku PET Centre
% Questions to Diana Bocancea at diiobo@utu.fi


top_dir      =  deblank(params.top_dir);
output_dir   =  sprintf('%s_RESULTS',top_dir);

if ~isdir(output_dir)
    mkdir(output_dir);
end


% Start diary file
diary(sprintf('%s\\diary_file.txt',output_dir))

% Error log file
log_filename = sapp_initialize_log_file(top_dir, output_dir);


fprintf('-------------------\n');
fprintf('SAPP working...\n');
fprintf('-------------------\n');


%% Retrieve subject information

datasets      = get_subfolders(top_dir)';

% get info from info.xls file and create subject folders
try
    subjects_info = sapp_get_subjects_info(datasets, top_dir, output_dir);
catch ME
    sapp_log_error(ME, log_filename)    % log error in file
    rethrow(ME)    % throws error to command window and terminates execution
end


%% Process Datasets

% For each dataset:
for i=1:length(datasets)
    
    try
        data_set = datasets{i};
    
        fprintf('\n\n-------------------\n');
        fprintf('Processing %s dataset\n', data_set);
        fprintf('-------------------\n\n');
    
        % Check if there are missing PET DICOM files
        if ~check_pet_files(data_set, top_dir)
            error('Missing Dicom files in PT_%s...Cannot convert to Nifti! Skipping %s dataset...', data_set, data_set);
        end
            
        % Convert dcm to nifti
        sapp_convert_dataset_to_nifti(data_set, top_dir);
        
        % Info of the subjects in data_set
        index = strcmp({subjects_info.dataset}, data_set) == 1;  

        % Coregister Whole Body PET to CT
        sapp_wholebody_coreg(data_set, top_dir, subjects_info(index));
        
        % Extract head(s)
        extract_head(data_set, subjects_info(index), top_dir, params.Species.species);
        
        
        fprintf('\n\n-------------------\n');
        fprintf('Dataset %s has been processed correctly\n', data_set);
        fprintf('-------------------\n\n');   
        
    catch ME
        warning('Something went wrong when processing DATASET %s. SAPP will skip it!', data_set)
        % log warning that dataset failed and the error message
        sapp_log_error(ME, log_filename, 'dataset', data_set)
    end
   
end

%%   Process Subjects

subjects = {subjects_info.name}';

% For each subject:
for k=1:length(subjects)
        
    fprintf('\n\n-------------------\n');
    fprintf('Processing Subject: %s\n', subjects{k});
    fprintf('-------------------\n\n');
    
    try
        % Reorient head and save backup files
        sapp_head_reorient(subjects_info(k));
        
        % Preprocess: coregistration PET-CT, spatial normalization to Atlas, QC
        sapp_single_subject_preprocess(subjects_info(k), params);
        
        % Atlas-based VOI analysis
        sapp_single_subject_analyze(subjects_info(k), params);
        
        % Create parametric images
        if params.parametricImage
            sapp_parametric_images(subjects_info(k), params);
        end
        
        fprintf('\n\n-------------------\n');
        fprintf('Subject %s has been processed and analysed correctly\n', subjects{k});
        fprintf('-------------------\n\n');        
        
    catch ME
        warning('Something went wrong when processing SUBJECT %s. SAPP will skip it!', subjects{k});
        % log warning that dataset failed and the error message
        sapp_log_error(ME, log_filename, 'subject', subjects{k})
        % clear the failed subject folder
        rmdir(subjects_info(k).path, 's')
    end
    
end



%% "Finished" notification

fprintf('--------------------------------------------------\n');
fprintf('SAPP Finalized processing and analysing!\n')
fprintf('Check the subjects in the following folder:\n%s\n', output_dir);
fprintf('--------------------------------------------------\n\n');

diary off

end

