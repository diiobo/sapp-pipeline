function varargout = sapp(varargin)
%SAPP MATLAB code file for sapp.fig
%      SAPP, by itself, creates a new SAPP or raises the existing
%      singleton*.
%
%      H = SAPP returns the handle to a new SAPP or the handle to
%      the existing singleton*.
%
%      SAPP('Property','Value',...) creates a new SAPP using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to sapp_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      SAPP('CALLBACK') and SAPP('CALLBACK',hObject,...) call the
%      local function named CALLBACK in SAPP.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sapp

% Last Modified by GUIDE v2.5 29-Jun-2018 13:45:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sapp_OpeningFcn, ...
                   'gui_OutputFcn',  @sapp_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sapp is made visible.
function sapp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for sapp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sapp wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% addpath(genpath('C:\SAPP_pipeline\SAPP'));
addpath('C:\SAPP_pipeline\spm12');

if strcmp(get(get(handles.uibuttongroup1,'SelectedObject'), 'Tag'), 'rat_radiobutton')
    set(handles.uipanel8, 'Visible', 'on');
    set(handles.uibuttongroup4, 'Visible', 'on');
end

if get(handles.parametricImage_checkbox,'Value')
    set(handles.staticParametricImage_checkbox, 'Visible', 'on');
end

if get(handles.staticParametricImage_checkbox,'Value')
    set(handles.uipanel13, 'Visible', 'on');
end



% --- Outputs from this function are returned to the command line.
function varargout = sapp_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in browse_button.
function browse_button_Callback(hObject, eventdata, handles)
% hObject    handle to browse_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

top_dir = deblank(uigetdir('C:\\DianaIB\\DATA\\' , 'Select Main Directory'));
if top_dir ~=0
    [~, folder, ~]=fileparts(top_dir);
    if isempty(get_subfolders(top_dir))
        errordlg('The selected folder is empty! Please select the folder with your data in the required format')
    else
        if isdir(top_dir) && ~isempty(folder)
            set(hObject,'UserData',top_dir);
            set(handles.directory_edit,'String',top_dir);
            set(handles.directory_edit,'UserData',top_dir);
        else
            set(handles.directory_edit,'String','Not valid');
        end
    end
    
end


%==========================================================================

% --- Executes on button press on start_button.

function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get parameters from GUI
params.top_dir      =   deblank(get(handles.directory_edit,'String'));
params.refROIs      =   get(handles.refRegions_listbox, 'Value') - 1;  % if 0, no reference region
params.avgFrames    =   [str2double(get(handles.first_frame_edit, 'String')),...
                         str2double(get(handles.last_frame_edit, 'String'))];

if strcmp(get(get(handles.uibuttongroup1,'SelectedObject'), 'Tag'), 'mouse_radiobutton')
   params.Species.species    = 'M';
   params.Species.atlas.mergedBrain = 1;  % ensure merged is selected
else
   params.Species.species    = 'R';
   
   if strcmp(get(get(handles.uibuttongroup4,'SelectedObject'), 'Tag'), 'SD_radiobutton')
       params.Species.strain = 'SD';
   else
       params.Species.strain = 'F';
   end
end

params.Species.atlas.mergedBrain = get(handles.merged_brain_checkbox, 'Value');
params.Species.atlas.splitBrain = get(handles.split_brain_checkbox, 'Value');


if strcmp(get(get(handles.uibuttongroup2,'SelectedObject'), 'Tag'), 'rigid_radiobutton')
   params.rigid    = 1; % rigid
else
   params.rigid    = 0; % affine
end

params.parametricImage       = get(handles.parametricImage_checkbox, 'Value');
params.staticParametricImage = get(handles.staticParametricImage_checkbox, 'Value');
params.parametricFrames      = [str2double(get(handles.parametricImage_firstFrame_edit, 'String')),...
                                str2double(get(handles.parametricImage_lastFrame_edit, 'String'))] ;
params.parametricImageType   = get(handles.parametricImage_type_listbox, 'Value');
params.parametricRefROI      = get(handles.parametricImage_refROI_listbox, 'Value') - 1;  % if 0, no reference region

% RUN
sapp_main(params);
%sapp_main_parametric_images_only(params);










%==========================================================================


% --- Executes on button press in extract_head_checkbox.
function extract_head_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to extract_head_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of extract_head_checkbox


% --- Executes on button press in coreg_checkbox.
function coreg_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to coreg_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of coreg_checkbox


% --- Executes on button press in mouse_radiobutton.
function mouse_radiobutton_Callback(hObject, eventdata, handles)
% hObject    handle to mouse_radiobutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject, 'Value')
    set(handles.uipanel8, 'Visible', 'off'); 
    set(handles.uibuttongroup4, 'Visible', 'off'); 
end
% Hint: get(hObject,'Value') returns toggle state of mouse_radiobutton


% --- Executes on button press in rat_radiobutton.
function rat_radiobutton_Callback(hObject, eventdata, handles)
% hObject    handle to rat_radiobutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rat_radiobutton
if get(hObject, 'Value')
    set(handles.uipanel8, 'Visible', 'on');
    set(handles.uibuttongroup4, 'Visible', 'on');
end


function directory_edit_Callback(hObject, eventdata, handles)
% hObject    handle to directory_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of directory_edit as text
%        str2double(get(hObject,'String')) returns contents of directory_edit as a double


% --- Executes during object creation, after setting all properties.
function directory_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to directory_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ratio_checkbox.
function ratio_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to ratio_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ratio_checkbox
if get(hObject, 'Value')
    %Get Species from the radiobuttons
    if strcmp(get(get(handles.uibuttongroup1,'SelectedObject'), 'Tag'), 'mouse_radiobutton')
        Species.species    = 'M';
    else
        Species.species    = 'R';
    end
    
    voi_data = read_voi_data(Species);
    voi_names = cellstr(voi_data{2});
    
    set(handles.refRegions_listbox,'string', {'Select reference ROIs:', voi_names{:}}');
    set(handles.refRegions_listbox, 'Visible', 'on');
    set(handles.refRegions_listbox, 'max', 5);  % Change how many reference regions you can select
    
else
    set(handles.refRegions_listbox, 'Visible', 'off');
    set(handles.refRegions_listbox, 'Value', 1);
end



% --- Executes on selection change in refRegions_listbox.
function refRegions_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to refRegions_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns refRegions_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from refRegions_listbox


% --- Executes during object creation, after setting all properties.
function refRegions_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to refRegions_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






function first_frame_edit_Callback(hObject, eventdata, handles)
% hObject    handle to first_frame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of first_frame_edit as text
%        str2double(get(hObject,'String')) returns contents of first_frame_edit as a double


% --- Executes during object creation, after setting all properties.
function first_frame_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to first_frame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function last_frame_edit_Callback(hObject, eventdata, handles)
% hObject    handle to last_frame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of last_frame_edit as text
%        str2double(get(hObject,'String')) returns contents of last_frame_edit as a double


% --- Executes during object creation, after setting all properties.
function last_frame_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to last_frame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in merged_brain_checkbox.
function merged_brain_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to merged_brain_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of merged_brain_checkbox


% --- Executes on button press in split_brain_checkbox.
function split_brain_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to split_brain_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of split_brain_checkbox


% --- Executes during object creation, after setting all properties.
function uibuttongroup1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uibuttongroup1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function uipanel8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in parametricImage_checkbox.
function parametricImage_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to parametricImage_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of parametricImage_checkbox

if get(hObject, 'Value')
    %Get Species from the radiobuttons
    if strcmp(get(get(handles.uibuttongroup1,'SelectedObject'), 'Tag'), 'mouse_radiobutton')
        Species.species    = 'M';
    else
        Species.species    = 'R';
    end
    
    % read ROI names from the atlas text file
    voi_data = read_voi_data(Species);
    voi_names = cellstr(voi_data{2});
    
    set(handles.parametricImage_refROI_listbox,'string', {'Select reference ROI:', voi_names{:}}');
    
    set(handles.staticParametricImage_checkbox, 'Visible', 'on');
    set(handles.parametricImage_type_listbox, 'Visible', 'on');
    set(handles.parametricImage_refROI_listbox, 'Visible', 'on');
else
    set(handles.staticParametricImage_checkbox, 'Visible', 'off');
    set(handles.parametricImage_type_listbox, 'Visible', 'off');
    set(handles.parametricImage_refROI_listbox, 'Visible', 'off');
    set(handles.uipanel13, 'Visible', 'off');

end


% --- Executes on button press in singleSubject_button.
function singleSubject_button_Callback(hObject, eventdata, handles)
% hObject    handle to singleSubject_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in staticParametricImage_checkbox.
function staticParametricImage_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to staticParametricImage_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of staticParametricImage_checkbox

if get(hObject, 'Value')
    set(handles.uipanel13, 'Visible', 'on');
else
    set(handles.uipanel13, 'Visible', 'off');
end



function parametricImage_firstFrame_edit_Callback(hObject, eventdata, handles)
% hObject    handle to parametricImage_firstFrame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of parametricImage_firstFrame_edit as text
%        str2double(get(hObject,'String')) returns contents of parametricImage_firstFrame_edit as a double


% --- Executes during object creation, after setting all properties.
function parametricImage_firstFrame_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parametricImage_firstFrame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function parametricImage_lastFrame_edit_Callback(hObject, eventdata, handles)
% hObject    handle to parametricImage_lastFrame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of parametricImage_lastFrame_edit as text
%        str2double(get(hObject,'String')) returns contents of parametricImage_lastFrame_edit as a double


% --- Executes during object creation, after setting all properties.
function parametricImage_lastFrame_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parametricImage_lastFrame_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in parametricImage_type_listbox.
function parametricImage_type_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to parametricImage_type_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns parametricImage_type_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from parametricImage_type_listbox


% --- Executes during object creation, after setting all properties.
function parametricImage_type_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parametricImage_type_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in parametricImage_refROI_listbox.
function parametricImage_refROI_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to parametricImage_refROI_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns parametricImage_refROI_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from parametricImage_refROI_listbox


% --- Executes during object creation, after setting all properties.
function parametricImage_refROI_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parametricImage_refROI_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
