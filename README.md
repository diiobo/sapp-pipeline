# SAPP pipeline

Small Animal PET/CT Pipeline. 

This project was part of my Master Thesis and it was done at the Preclinical Imaging laboratory, Turku PET Centre. The goal 
was to design, implement and test a pipeline for automating the analysis of mouse and rat brain PET/CT images.

The source code is a working prototype written in Matlab on top of SPM12. 
