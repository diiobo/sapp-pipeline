function sapp_def = sapp_defaults(Species)  

switch Species.species
    
    case 'M'  % Mouse
        sapp_def.species    = 'M';
        sapp_def.atlas_dir = 'C:\\SAPP_pipeline\\SAPP_atlas\\Mouse_atlas';
        sapp_def.ct_ref    = sprintf('%s\\templates\\Mouse_meanCT_SMALLbbox.nii' , sapp_def.atlas_dir);
        sapp_def.mri       = sprintf('%s\\templates\\Mouse_C57BL6_MRI_masked.nii' , sapp_def.atlas_dir);
        sapp_def.mask      = sprintf('%s\\mask\\brainmask_resized.nii' , sapp_def.atlas_dir);
        sapp_def.atlas     = sprintf('%s\\VOIs\\Mouse_C57BL6_Atlas.nii' , sapp_def.atlas_dir);
        sapp_def.voi       = sprintf('%s\\VOIs\\rMouse_C57BL6_Atlas_vois_02.nii' , sapp_def.atlas_dir);
        
        sapp_def.bound     = sprintf('%s\\boundaries\\vois_boundary.nii' , sapp_def.atlas_dir);

        
    case 'R'  % Rat
        sapp_def.species    = 'R';
        sapp_def.atlas_dir = 'C:\\SAPP_pipeline\\SAPP_atlas\\Rat_atlas';
        
        switch Species.strain
            
            case 'SD'  % Sprague-Dawley
                sapp_def.ct_ref    = sprintf('%s\\templates\\Rat_meanCT_bbox.nii' , sapp_def.atlas_dir);
                sapp_def.mri       = sprintf('%s\\templates\\Rat_Schiffer_T2.nii' , sapp_def.atlas_dir);
                sapp_def.mask      = sprintf('%s\\mask\\brainmask_resized.nii' , sapp_def.atlas_dir);
                sapp_def.bound     = sprintf('%s\\boundaries\\brainmask_boundary.nii' , sapp_def.atlas_dir);
                
                sapp_def.atlas     = sprintf('%s\\VOIs\\Rat_Schiffer_Atlas_WB.nii' , sapp_def.atlas_dir);
                sapp_def.voi       = sprintf('%s\\VOIs\\Rat_Schiffer_Atlas_vois_WB_resized.nii' ,sapp_def.atlas_dir);  % 5% smaller
                
                                
                
            case 'F'  % Fisher
                % sapp_def.ct_ref    =
                % sapp_def.mri       =
                % sapp_def.mask      =
                % sapp_def.atlas     =
                % sapp_def.voi       =
                % sapp_def.bound     =
                
                
                
        end
        
end
end